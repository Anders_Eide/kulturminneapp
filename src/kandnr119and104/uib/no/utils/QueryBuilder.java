package kandnr119and104.uib.no.utils;

/**
 * Utility class used to store sparql queries in order to abstract them away from the
 * rest of the logic
 * @author Candidate number: 119 & 104
 *
 */
public class QueryBuilder {

	//Declaring commonly used prefixes
	private static final String erlangenBase = "<http://erlangen-crm.org/current/";
	private final static String schemaBase = "<https://schema.org/";

	/**
	 * Returns a string representation of a sparql query that upon execution will return
	 * any items that have the same creator as the input item
	 * @param itemURI the unique URI of the item you wish to search with
	 */
	public static String getItemsOfSameCreatorAsItem(String itemURI) {
		return  "SELECT DISTINCT ?item ?title ?creator WHERE "
	    		+ "{"
	    		+ 		itemURI +  erlangenBase + "P135i_was_created_by> ?creator ."
	    		+ "     ?item " + erlangenBase + "P135i_was_created_by> ?creator . "
	    		+ "     ?item " + erlangenBase + "P102_has_title> ?title"
	    		+ "}"
	    		+ "";
	}

	/**
	 * Returns a string representation of a sparql query that upon execution will return
	 * any items that share the same creator or type
	 * @param itemURI the unique URI of the item you wish to search with
	 */
	public static String getSimilarItemsQuery(String itemURI){

		return "SELECT DISTINCT ?item ?title ?creator WHERE"
				+ "{"
				+ itemURI + " " + erlangenBase + "P135i_was_created_by> ?creator; "
				+ 	 			erlangenBase + "P2_has_type> ?type; "
				+ 				erlangenBase + "P102_has_title> ?title "
				+ "{ ?item " + erlangenBase + "P135i_was_created_by> ?creator}"
				+ "UNION"
				+ "{ ?item " + erlangenBase + "P2_has_type> ?type.}"
				+ "}";
	}

	/**
	 * Returns a string representation of a sparql query that upon execution will return
	 * any items that are located within the specified radius of the input coordinates
	 * @param latitude the latitude to search around
	 * @param longitude the longitude to search around
	 * @param radius the size of the radius in degrees
	 */
	public static String getNearbyItems(double latitude, double longitude, double radius){

		return    "PREFIX xsd:     <http://www.w3.org/2001/XMLSchema#> "
				+ "SELECT DISTINCT ?item ?title ?creator ?latitude ?longitude WHERE"
				+ "{"
				+ "?item " + schemaBase + "contentLocation> ?location;"
				+ 			 erlangenBase + "P135i_was_created_by> ?creator;"
				+			 erlangenBase + "P102_has_title> ?title."
				+ "?location " + schemaBase + "geo> ?coordinates."
				+ "?coordinates " + schemaBase + "latitude> ?latitude."
				+ "?coordinates " + schemaBase + "longitude> ?longitude."
				+ "FILTER ("
				+ "xsd:double(?latitude) > xsd:double(\"" + (latitude - radius) + "\") && xsd:double(?latitude) < xsd:double(\"" + (latitude + radius) + "\") "
				+ "&& xsd:double(?longitude) > xsd:double(\"" + (longitude - radius) + "\") && xsd:double(?longitude) < xsd:double(\"" + (longitude + radius) +"\") "
				+ ")}";
	}

	/**
	 * Returns a string representation of a sparql query that upon execution will return
	 * any items that are located at the specified location
	 * @param location The location to search for items in, such as 'Oslo' or 'Bergen'
	 */
	public static String getItemsFromLocation(String location) {
		return    "SELECT DISTINCT ?item ?title ?creator WHERE"
				+ "{"
				+ "?item " + schemaBase + "contentLocation> ?location; "
				+ 		 	 erlangenBase + "P135i_was_created_by> ?creator; "
				+			 erlangenBase + "P102_has_title> ?title. "
				+ "?location " + schemaBase + "geographicName> \"" + location + "\". "
				+ "}";
	}
}
