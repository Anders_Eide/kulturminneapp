package kandnr119and104.uib.no;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import kandnr119and104.uib.no.gui.controller.SemanticScreenController;
import kandnr119and104.uib.no.io.Item;
import kandnr119and104.uib.no.io.Parser;
import kandnr119and104.uib.no.lifter.SemanticLifter;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;

import kandnr119and104.uib.no.io.Parser;
import kandnr119and104.uib.no.lifter.SemanticLifter;

/**
 * The main class of the application. 
 * @author Candidate number: 119 & 104
 */
@SuppressWarnings("unused")
public class Main extends Application {

    private static Model model;
    private static Dataset dataset;


    public static void main(String[] args) throws IOException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //Setting up the dataset
        dataset = TDBFactory.createDataset("kulturarvdata/");
        model = dataset.getDefaultModel();
        SemanticLifter lifter = new SemanticLifter(model);

        SemanticScreenController controller = new SemanticScreenController(dataset);

        //Utility methods for manipulating the dataset
//       model.removeAll();
//       List<Item> items = getItems();
//       lifter.liftResources(items);

        //Gui stuff
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/kandnr119and104/uib/no/gui/semanticScreen.fxml"));
        loader.setController(controller);
        Parent root = loader.load();

        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.show();

    }

    /**
     * Utility method for demonstrating the json parser used to gather data from
     * the Norvegiana APIs
     * @return A List of items
     * @throws IOException
     */
    private static List<Item> getItems() throws IOException {
        ArrayList<Item> items = new ArrayList<>();

        items.addAll(Parser.parseFromURL("http://kulturnett2.delving.org/api/search?query=delving_spec:difo%20AND%20abm_municipality_text:Oslo&format=json"));
        items.addAll(Parser.parseFromURL("http://kulturnett2.delving.org/api/search?query=delving_spec:DiMu%20AND%20abm_municipality_text:Oslo&format=json"));
        items.addAll(Parser.parseFromURL("http://kulturnett2.delving.org/api/search?query=delving_spec:Kulturminnesok%20AND%20abm_municipality_text:Oslo&format=json"));
        items.addAll(Parser.parseFromURL("http://kulturnett2.delving.org/api/search?query=*%3A*&qf=abm_category_text%3Akunst&pt=59.910586%2C10.734329&d=5&format=json"));
        items.addAll(Parser.parseFromURL("http://kulturnett2.delving.org/api/search?query=delving_spec%3ADiMu&format=json"));
        items.addAll(Parser.parseFromFile("res/DiMusøk.json"));
        items.addAll(Parser.parseFromFile("res/Kulturminnesøk.json"));
        items.addAll(Parser.parseFromFile("res/Difosøk.json"));
        return items;
    }

    /**
     * Utility method for printing the contents of the dataset
     */
    public static void printModel(){
        System.out.println("--Model Start--");
        model.write(System.out, "TURTLE");
    }

    /**
     * Method for running various queries against the dataset
     * @param queryString The query to run
     */
    private static void runQuery(String queryString) {
        Query query = QueryFactory.create(queryString);
        QueryExecution queryExecution = QueryExecutionFactory.create(query, dataset);

        try{
            ResultSet resultSet = queryExecution.execSelect();
            System.out.println("-----Query Result-----" + "\n");

            int result = 0;
            while (resultSet.hasNext()) {
                System.out.println(resultSet.next());
                result++;
            }

            System.out.println("Number of results: " + result);

        } catch (Exception ignored){
        }
    }

}
