package kandnr119and104.uib.no.lifter;

import java.util.List;

import kandnr119and104.uib.no.io.Item;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Class used to semantically lift resources parsed by the parser
 * and generate triples to be stored in the TDB
 *
 * @author Candidate number: 119 & 104
 */

public class SemanticLifter {

	//Storing commonly used prefixes as fields
	private final static String erlangenBase = "http://erlangen-crm.org/current/";
	private final static String dc_coreBase = "http://purl.org/dc/terms/";
	private final static String schemaBase = "https://schema.org/";

	private final Model model;

	public SemanticLifter(Model model){
		this.model = model;
	}

	/**
	 * Generating triples of the data and committing them to a TDB
	 * @param items is the values in the list of parsed data
	 */
	public void liftResources(List<Item> items) {
		//Opening the model for changes
		model.begin();

		//Creating properties for connecting nodes holding geolocational information
		//to the nodes representing their respective items
		Property placeProperty = model.createProperty(schemaBase + "contentLocation");
		Property coordProperty = model.createProperty(schemaBase + "geo");
		
		Property latitude = model.createProperty(schemaBase + "latitude");
		Property longitude = model.createProperty(schemaBase + "longitude");

		for(Item item : items){
			//Creating resources to hold information about our item, two of which are blank
			Resource itemResource = model.createResource(item.isShownAtURI.get(0));
			Resource placeResource = model.createResource();
			Resource coordResource = model.createResource();

			//Assigning predicates to our item
			addValuesToProperty(itemResource, erlangenBase + "P78_is_identified_by", item.isShownAtURI);
			addValuesToProperty(itemResource, dc_coreBase + "description", item.description);
			addValuesToProperty(itemResource, erlangenBase + "P65_is_shown_by", item.imageURI);

			addValuesToProperty(itemResource, erlangenBase + "P4_has_time-span", item.date);
			addValuesToProperty(itemResource, erlangenBase + "P52_has_current_owner", item.provider);
			addValuesToProperty(itemResource, erlangenBase + "P102_has_title", item.title);

			addValuesToProperty(itemResource, erlangenBase + "P135i_was_created_by", item.creators);
			addValuesToProperty(itemResource, erlangenBase + "P2_has_type", item.subjects);
			addValuesToProperty(itemResource, erlangenBase + "P138i_has_representation", item.types);
			
			//Connecting the two blank nodes to our item node and assigning them information
			//about the item's location
			itemResource.addProperty(placeProperty, placeResource);
			addValuesToProperty(placeResource, schemaBase + "geographicName", item.namedPlace);
			
			placeResource.addProperty(coordProperty, coordResource);
			coordResource.addProperty(latitude, item.latitude + "");
			coordResource.addProperty(longitude, item.longitude + "");
		}
		model.commit();
	}

	/**
	 * Utility method for assigning properties to a resource from a list of values
	 * @param itemResource the current resource
	 * @param propertyName the name of the property (e.g. subjects)
	 * @param values the values from the parsed data
	 */
	private void addValuesToProperty(Resource itemResource, String propertyName, List<String> values){
		if (values != null) {
			Property property = model.createProperty(propertyName);
			
			for (String value : values) {
				itemResource.addProperty(property, value);
			}
		}
	}
}

