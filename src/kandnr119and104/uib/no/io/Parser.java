package kandnr119and104.uib.no.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Parser used to parse the json outputs from the Norvegiana api
 * both directly from a URL as well as from json files
 * @author Candidate number: 119 & 104
 */

public class Parser {

	private static JsonObject fields;

	/**
	 * Method used to parse items from an input url
	 * @param stringURL The url to parse items from
	 * @return A list of Item
	 * @throws IOException
	 */
	public static List<Item> parseFromURL(String stringURL) throws IOException {
		BufferedReader streamReader = getBufferedReader(stringURL);
		JsonArray jsonItems = getJsonItems(streamReader);

		ArrayList<Item> parsedItems = parseItems(jsonItems);
		return parsedItems;
	}

	/**
	 * Method used to parse items from a saved file (in JSON format)
	 * @param filePath the filepath of the file, relative to the project folder/jar file
	 * @return A list of the items contained in the file
	 * @throws IOException
	 */
	public static List<Item> parseFromFile(String filePath) throws IOException {
		File jsonFile = new File(filePath);

		FileInputStream inputStream = new FileInputStream(jsonFile);
		BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));

		JsonArray jsonItems = getJsonItems(fileReader);
		ArrayList<Item> parsedItems = parseItems(jsonItems);

		return parsedItems;
	}

	/**
	 * Internal method used to parse out the contents of the JsonArray containing
	 * the actual items
	 */
	private static ArrayList<Item> parseItems(JsonArray jsonItems) {
		Iterator<JsonElement> elementIterator = jsonItems.iterator();

		ArrayList<Item> parsedItems = new ArrayList<Item>();

		while(elementIterator.hasNext()){
			Item item = parseElement(elementIterator.next());
			parsedItems.add(item);
		}
		return parsedItems;
	}

	/**
	 * Internal method used to locate the JsonArray containing the actual items
	 */
	private static JsonArray getJsonItems(BufferedReader streamReader) {
		JsonParser jsonParser = new JsonParser();

		JsonElement element = jsonParser.parse(streamReader);

		JsonObject rootObject = element.getAsJsonObject();
		JsonObject resultObject = rootObject.get("result").getAsJsonObject();
		return resultObject.get("items").getAsJsonArray();
	}

	/**
	 * Internal method for setting up a BufferedReader to read the
	 * contents of a url
	 * @throws IOException
	 */
	private static BufferedReader getBufferedReader(String stringURL) throws IOException {
		URL url = new URL(stringURL);
		InputStream stream = url.openStream();

		return new BufferedReader(new InputStreamReader(stream, "UTF-8"));
	}

	/**
	 * Method for parsing items from specific json elements
	 * @param element a JsonElement containing an Item object
	 * @return an instance of Item with the field values corresponding to the
	 * values
	 */
	private static Item parseElement(JsonElement element){
		fields = getItemFields(element);

		//Hacky solution to determine which items lack coordinates
		double latitude = 999.0;
		double longitude = 999.0;

		//Parsing the values for each of the item's fields
		List<String> itemCreators = parseFieldValues("dc_creator");
		List<String> itemSubjects = parseFieldValues("dc_subject");
		List<String> itemTypes = parseFieldValues("dc_type");
		List<String> itemDescription = parseFieldValues("dc_description");

		List<String> itemProvider = parseFieldValues("europeana_dataProvider");
		List<String> itemTitle = parseFieldValues("dc_title");
		List<String> itemImageURI = parseFieldValues("abm_imageuri");
		List<String> itemPageURI = parseFieldValues("europeana_isShownAt");
		List<String> itemDate = parseFieldValues("dc_date");
		List<String> itemNamedPlace = parseFieldValues("abm_namedPlace");

		List<String> itemCoordinates = parseFieldValues("abm_latLong");
		List<String> itemMunicipality = parseFieldValues("abm_municipality");

		//Used to fetch coordinates for items which lack coordinates
		//but contains information about a named place
		if (itemCoordinates == null && itemMunicipality != null) {
			itemCoordinates = new ArrayList<String>();
			itemCoordinates.add(GeonamesQueryer.getCoordsOfNamedPlace(itemMunicipality.get(0)));
		}

		//Various checks to handle fields having differing names in different datasets

		if(itemNamedPlace == null) {
			itemNamedPlace = itemMunicipality;
		}

		if(itemTypes == null)
			itemTypes = parseFieldValues("abm_type");

		if(itemDate == null)
			itemDate = parseFieldValues("dcterms_created");

		if (itemImageURI == null)
			itemImageURI = parseFieldValues("delving_thumbnail");

		if(itemCoordinates != null){
			String[] coords = itemCoordinates.get(0).split(",");
			latitude = Double.parseDouble(coords[0]);
			longitude = Double.parseDouble(coords[1]);
		}

		if (itemCreators == null){
			itemCreators = new ArrayList<>();
			itemCreators.add("Ukjent");
		}

		return new Item(itemTitle, itemImageURI, itemPageURI, itemNamedPlace, itemDate, itemProvider,
				itemDescription, itemSubjects, itemTypes, itemCreators, latitude, longitude);
	}

	/**
	 * Returns the JsonObject containing the JsonArray of fields
	 * for the Item.
	 */
	private static JsonObject getItemFields(JsonElement element) {
		JsonObject jsonItem = element.getAsJsonObject();
		JsonObject item = jsonItem.get("item").getAsJsonObject();
		return item.get("fields").getAsJsonObject();
	}

	/**
	 * Help method for individual item fields as JsonArrays by name
	 * @param fieldName the name of the field to be parsed e.g "dc_creator"
	 */
	private static List<String> parseFieldValues(String fieldName) {
		List<String> fieldStringList = new ArrayList<String>();
		JsonArray fieldArray = (JsonArray) fields.get(fieldName);
		if (fieldArray != null) {
			for (JsonElement element : fieldArray) {
				fieldStringList.add(element.getAsString());
			}
			return fieldStringList;
		}
		return null;
	}

}
