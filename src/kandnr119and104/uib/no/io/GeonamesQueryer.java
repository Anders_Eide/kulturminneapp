package kandnr119and104.uib.no.io;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Class used to query Geonames for the latitude and longitude of named places
 * @author Candidate number: 119 & 104
 */
public class GeonamesQueryer {

    private static final String baseQueryURLstart = "http://api.geonames.org/searchJSON?name_equals=";
    private static final String baseQueryURLfinal = "&country=NO&maxRows=1&username=gunnar&type=json";

    /**
     * Main method for retrieving the coordinates of a named place
     * @param locationName is the name of the location to find coordinates for (from the main parser)
     * @return the coordinates of the place in lat/long
     */
    public static String getCoordsOfNamedPlace(String locationName) {
        String queryString = baseQueryURLstart + locationName.toLowerCase() + baseQueryURLfinal;
        String latlong = "";

        try {
            URL queryURL = new URL(queryString);
            InputStream inputStream = queryURL.openStream();
            BufferedReader queryResultReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            JsonObject result = getJsonItems(queryResultReader);
            if(result == null)
                return null;
            String latitude = result.get("lat").getAsString();
            String longitude = result.get("lng").getAsString();

            latlong = latitude + "," + longitude;

        } catch (IOException e){
            e.printStackTrace();
        }
        return latlong;
    }

    /**
     * Internal method for locating the JsonObject containing the results
     * of the query
     */
    private static JsonObject getJsonItems(BufferedReader streamReader) {
        JsonParser jsonParser = new JsonParser();

        JsonElement element = jsonParser.parse(streamReader);

        JsonObject rootObject = element.getAsJsonObject();
        JsonArray resultArray = rootObject.get("geonames").getAsJsonArray();
        if(resultArray.get(0) == null)
            return null;
        return resultArray.get(0).getAsJsonObject();
    }
}
