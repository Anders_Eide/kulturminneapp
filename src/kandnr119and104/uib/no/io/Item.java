package kandnr119and104.uib.no.io;

import java.io.Serializable;
import java.util.List;

/**
 * Data transfer object made to transfer information about items
 * @author Candidate number: 119 & 104
 *
 */
public class Item implements Serializable{

	private static final long serialVersionUID = 1L;
	public List<String> title, description, imageURI, isShownAtURI, namedPlace,
            date, provider, subjects, creators, types;
	public double latitude, longitude;

    public Item(List<String> title, List<String> imageURI, List<String> pageURI, List<String> namedPlace, List<String> date,
                List<String> provider, List<String> description, List<String> subjects, List<String> types, List<String> creators, double latitude, double longitude) {
        this.title = title;
        this.imageURI = imageURI;
        this.isShownAtURI = pageURI;
        this.namedPlace = namedPlace;
        this.date = date;
        this.provider = provider;
        this.description = description;
        this.subjects = subjects;
        this.types = types;
        this.creators = creators;
        
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
