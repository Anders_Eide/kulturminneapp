package kandnr119and104.uib.no.gui.controller;

import java.awt.Desktop;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import kandnr119and104.uib.no.utils.QueryBuilder;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;

/**
 * 
 * @author Candidate number: 119 & 104
 * This is the controller class for the FXML-document, which is drawing the GUI.
 */
public class SemanticScreenController implements Initializable {
	
	private List<String> values = new ArrayList<String>();
	private ObservableList<String> results;
    private static Dataset dataset;
    private Pattern pattern;
	private Matcher matcher;
	
	public SemanticScreenController(Dataset dataset) {
		SemanticScreenController.dataset = dataset;
	}
	

	@FXML
	ToggleGroup rbGroup;
	
	@FXML
	ListView<String> resultView = new ListView<>();
	
	@FXML
	RadioButton nearByRadio;
	
	@FXML
	RadioButton locationRadio;
	
	@FXML
	RadioButton similarRadio;

	@FXML
	RadioButton creatorRadio;
	
	@FXML
	private void handleListViewClick(MouseEvent event) throws Exception {
		if(event.getClickCount() == 2) {
			String unRefinedURL = resultView.getSelectionModel().getSelectedItem();
			String refinedURL = null;
			
			pattern = Pattern.compile("<(\\S+)>");
			matcher = pattern.matcher(unRefinedURL);

			if (matcher.find()) {
				System.out.println(matcher.group(1));
				refinedURL = matcher.group(1);
			}
			
				Desktop.getDesktop().browse(new URL(refinedURL).toURI());
			} 
		}

	@FXML
	private void handleExitButtonEvent(MouseEvent e) {
		System.exit(0);
	}
	
	@FXML
	private void handleSearchButtonEvent(MouseEvent e) throws Exception {
		//TODO fill with correct data
		
		if(nearByRadio.isSelected()) {
			runQuery(QueryBuilder.getNearbyItems(59.0, 11.0, 1));
			fillListViewWithData();
		} else if(locationRadio.isSelected()) {
			runQuery(QueryBuilder.getItemsFromLocation("Oslo"));
			fillListViewWithData();
		} else if(similarRadio.isSelected()) {
			runQuery(QueryBuilder.getSimilarItemsQuery("<http://digitaltfortalt.no/things/thing/H-DF/DF.6370>"));
			fillListViewWithData();
		} else {
			runQuery(QueryBuilder.getItemsOfSameCreatorAsItem("<http://digitaltmuseum.no/things/thing/NSK/SJF-F.003499>"));
			fillListViewWithData();
		}
	}
	/**
	 * Fill the ListView with the Data
	 */
	public void fillListViewWithData() {
		results = FXCollections.observableArrayList(values);
		resultView.setItems(results);
	}
	
    /**
     * Method for running various queries against the dataset
     * @param queryString The query to run
     */
    private void runQuery(String queryString) {
        Query query = QueryFactory.create(queryString);
        QueryExecution queryExecution = QueryExecutionFactory.create(query, dataset);
        values.clear();
        try{
            ResultSet resultSet = queryExecution.execSelect();
            System.out.println("-----Query Result-----" + "\n");

            int result = 0;
            while (resultSet.hasNext()) {
            	values.add(resultSet.next().toString());
                result++;
            }

            System.out.println("Number of results: " + result);

        } catch (Exception e){
        	e.printStackTrace();
        	System.out.println("What");
        }
    }
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

}
